package com.wartatv.yukantree.model;

import com.google.gson.annotations.SerializedName;

public class Stat{

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("updated_by")
	private Object updatedBy;

	@SerializedName("description")
	private Object description;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	@SerializedName("created_by")
	private Object createdBy;

	@SerializedName("deleted_at")
	private Object deletedAt;

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public Object getUpdatedBy(){
		return updatedBy;
	}

	public Object getDescription(){
		return description;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}

	public Object getCreatedBy(){
		return createdBy;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}
}