package com.wartatv.yukantree.model;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("stat")
	private Stat stat;

	@SerializedName("address")
	private String address;

	@SerializedName("loketimage")
	private String loketimage;

	@SerializedName("city")
	private String city;

	@SerializedName("description")
	private Object description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("loketname")
	private String loketname;

	@SerializedName("statusname")
	private String statusname;

	@SerializedName("loketId")
	private int loketId;

	@SerializedName("type")
	private int type;

	@SerializedName("userId")
	private int userId;

	@SerializedName("deleted_at")
	private Object deletedAt;

	@SerializedName("number")
	private String number;

	@SerializedName("hostname")
	private String hostname;

	@SerializedName("loket")
	private Loket loket;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("phone")
	private String phone;

	@SerializedName("categoryname")
	private String categoryname;

	@SerializedName("id")
	private String id;

	@SerializedName("tanggal")
	private String tanggal;

	@SerializedName("user")
	private User user;

	@SerializedName("status")
	private int status;

	@SerializedName("username")
	private String username;


	public DataItem() {
	}


	public DataItem(String id, String hostname, String address,String city,String loketname, String loketimage, String number,String statusname) {
		this.id = id;
		this.hostname = hostname;
		this.address = address;
		this.city = city;
		this.loketname = loketname;
		this.loketimage = loketimage;
		this.number = number;
		this.statusname = statusname;

	}

	public Stat getStat(){
		return stat;
	}

	public String getAddress(){
		return address;
	}

	public String getLoketimage(){
		return loketimage;
	}

	public String getCity(){
		return city;
	}

	public Object getDescription(){
		return description;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getLoketname(){
		return loketname;
	}

	public String getStatusname(){
		return statusname;
	}

	public int getLoketId(){
		return loketId;
	}

	public int getType(){
		return type;
	}

	public int getUserId(){
		return userId;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}

	public String getNumber(){
		return number;
	}

	public String getHostname(){
		return hostname;
	}

	public Loket getLoket(){
		return loket;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getPhone(){
		return phone;
	}

	public String getCategoryname(){
		return categoryname;
	}

	public String getId(){
		return id;
	}

	public String getTanggal(){
		return tanggal;
	}

	public User getUser(){
		return user;
	}

	public int getStatus(){
		return status;
	}

	public String getUsername(){
		return username;
	}
}