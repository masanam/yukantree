package com.wartatv.yukantree.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wartatv.yukantree.R;
import com.wartatv.yukantree.model.DataItem;

import java.util.List;

/**
 * Created by .
 * www.wartatv.com
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private List<DataItem> historyList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView hostTitle, address, city;
        public TextView loketName, number, status;

        ImageView imageView;
        ProgressBar progressBar;
        LinearLayout quantity_ll;
        TextView plus, minus, quantity;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            hostTitle = (TextView) itemView.findViewById(R.id.host_title);
            address = (TextView) itemView.findViewById(R.id.host_address);
            city = (TextView) itemView.findViewById(R.id.host_city);
            imageView = itemView.findViewById(R.id.product_image);
            progressBar = itemView.findViewById(R.id.progressbar);
            cardView = itemView.findViewById(R.id.card_view);

            loketName = itemView.findViewById(R.id.host_loket);
            number = itemView.findViewById(R.id.host_number);
            status = itemView.findViewById(R.id.host_status_history);

        }
    }


    public HistoryAdapter(List<DataItem> historyList) {
        this.historyList = historyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_new_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataItem movie = historyList.get(position);
        holder.number.setText(movie.getNumber());
        holder.loketName.setText(movie.getLoketname());
        holder.status.setText(movie.getStatusname());
        holder.hostTitle.setText(movie.getHostname());
        holder.city.setText(movie.getCity());

        if(movie.getStatusname().equals("Finished"))
        {
            holder.status.setBackgroundColor(Color.RED);
        }

        if(movie.getStatusname().equals("Cancelled"))
        {
            holder.status.setBackgroundColor(Color.DKGRAY);
        }
//
        Picasso.get().load(movie.getLoketimage()).error(R.drawable.no_image).into(holder.imageView, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                Log.d("Error : ", e.getMessage());
            }
        });

    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }
}