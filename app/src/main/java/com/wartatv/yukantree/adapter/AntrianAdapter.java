package com.wartatv.yukantree.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wartatv.yukantree.R;
import com.wartatv.yukantree.activity.ProductViewActivity;
import com.wartatv.yukantree.activity.QueueActivity;
import com.wartatv.yukantree.model.DataItem;
import com.wartatv.yukantree.model.Loket;

import java.util.List;

/**
 * Created by .
 * www.wartatv.com
 */
public class AntrianAdapter extends RecyclerView.Adapter<AntrianAdapter.MyViewHolder> {
    private List<DataItem> antrianList;
    Context context;
    String Tag;

    public AntrianAdapter(List<DataItem> antrianList, Context context) {
        this.antrianList = antrianList;
        this.context = context;
    }

    public AntrianAdapter(List<DataItem> antrianList, Context context, String tag) {
        this.antrianList = antrianList;
        this.context = context;
        Tag = tag;
    }

    public AntrianAdapter(List<DataItem> antrianList) {
        this.antrianList = antrianList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userName, number, status, refreshBtn;

        ImageView imageView;
        ProgressBar progressBar;
        LinearLayout quantity_ll;
        TextView plus, minus, quantity;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.host_number);
            status = itemView.findViewById(R.id.host_status);
            userName = itemView.findViewById(R.id.host_username);
            cardView = itemView.findViewById(R.id.card_view);
            refreshBtn = itemView.findViewById(R.id.refreshBtn);

        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_antrian, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataItem movie = antrianList.get(position);
        holder.number.setText(movie.getNumber());
        holder.status.setText(movie.getStatusname());
        holder.userName.setText(movie.getUsername());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QueueActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return antrianList.size();
    }
}