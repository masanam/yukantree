# YukAntree
## Overview

<p align="center">
  <img src="/images/banner.png" alt="Banner">
</p>

_YukAntree_ is Marketplace for Queue System. People can get queue number from the Host.Host can maintain the queue list.

## Features

<p align="center">
  <img src="/images/features.png" alt="Features">
</p>

Features that helps people to get the queue number. They are : 
1. Login/Sign Up
2. View and search facility list
3. See detail info
4. Get queue number
5. Queue history
6. Edit profile




